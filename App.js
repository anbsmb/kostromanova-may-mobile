import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
  Button,
} from "react-native";
import { WebView } from "react-native-webview";
import { StatusBar } from "expo-status-bar";
import * as Font from "expo-font";
import logo from "./assets/icons/logo.png";
import notif from "./assets/icons/notif.png";
import phigh from "./assets/icons/phigh.png";
import pmedium from "./assets/icons/pmedium.png";
import plow from "./assets/icons/plow.png";

export default class MainPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      getMap: "http://134.209.236.228:10003",
    };
  }

  async componentDidMount() {
    await this._loadAssets();
    //await this.getMap();
  }

  async _loadAssets() {
    await Font.loadAsync({
      "SFProDisplay-Bold": require("./assets/fonts/SFProDisplay-Bold.ttf"),
      "SFProDisplay-Medium": require("./assets/fonts/SFProDisplay-Medium.ttf"),
      "SFProDisplay-Regular": require("./assets/fonts/SFProDisplay-Regular.ttf"),
      "SFProDisplay-Semibold": require("./assets/fonts/SFProDisplay-Semibold.ttf"),
    });
  }

  async getMap() {
    fetch("http://134.209.236.228:8150/api/mobile/startMap?userId=9")
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        console.log(data);
      });
  }

  render() {
    return (
      <WebView
        originWhitelist={["*"]}
        source={{ uri: this.state.getMap }}
        style={{ width: "100%", height: "100%" }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bar: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "white",
  },
  bar_logo: {
    width: 30,
    height: 30,
    marginTop: 15,
    marginLeft: 15,
    color: "black",
  },
  bar_text: {
    fontSize: 20,
    marginTop: 15,
    fontWeight: "bold",
    color: "#444444",
    fontFamily: "SFProDisplay-Semibold",
  },
  bar_notif: {
    width: 30,
    height: 30,
    marginTop: 15,
    marginRight: 15,
  },
  body: {
    flex: 8,
    backgroundColor: "white",
    paddingLeft: 14,
    paddingRight: 14,
  },
  card: {
    flex: 1,
    backgroundColor: "white",
    marginTop: 6,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.08,
    shadowRadius: 2,
    elevation: 8,
    borderRadius: 8,
    paddingRight: 16,
    paddingLeft: 16,
    paddingTop: 8,
    paddingBottom: 8,
  },
  card_bar_priorityimage: {
    width: 24,
    height: 24,
    marginRight: 2,
  },
  card_bar_time: {
    color: "#444444",
    fontSize: 14,
    fontFamily: "SFProDisplay-Regular",
  },
  card_body_title: {
    color: "#444444",
    fontFamily: "SFProDisplay-Bold",
    marginBottom: 6,
  },
  card_body_created_title: {
    color: "#666666",
    fontFamily: "SFProDisplay-Regular",
  },
  card_body_created: {
    color: "#666666",
    fontFamily: "SFProDisplay-Bold",
  },
  card_body_desc: {
    color: "#444444",
  },
});
